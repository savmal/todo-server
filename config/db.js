const mongoose = require('mongoose');
const config = require('config');
const db = config.get('mongoURI');

const connectDB = async () => {
    try {
        await mongoose.connect(db, {
            //useNewUrlParser: true,
            //useUnifiedTopology: true
        });

        //mongoose.connect(process.env.DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });
        //const connection = mongoose.connection;

        //connection.once('open', function() {
        //    console.log("MongoDB database connection established successfully");
        //})

        console.log('MongoDB Connected...');
    } catch(err) {
        console.log(err.message);
        // Exit process with failure
        process.exit(1);
    }
}

module.exports = connectDB;