require('dotenv').config();
const express = require('express');
const app = express();
const connectDB = require('./config/db');
const bodyParser = require('body-parser');
const cors = require('cors');
const todoRoutes = express.Router();
const PORT = process.env.PORT || 4000;

let Todo = require('./todo.model');

app.use(cors());
app.use(bodyParser.json());

// Connect Database
connectDB();

todoRoutes.route('/').get(function(req, res) {
    Todo.find(function(err, todos) {
        if (err) {
            console.log(err);
        } else {
            res.json(todos);
        }
    });
});

todoRoutes.route('/:id').get(function(req, res) {
    let id = req.params.id;
    Todo.findById(id, function(err, todo) {
        res.json(todo);
    });
});

todoRoutes.route('/add').post(function(req, res) {
    let todo = new Todo(req.body);
    todo.save()
        .then(todo => {
            res.status(200).json( { 'todo': 'todo added successfully' } );
        })
        .catch(err => {
            res.status(400).send('adding new todo failed');
        });
});

todoRoutes.route('/update/:id').post(function(req, res) {
    Todo.findById(req.params.id, function(err, todo) {
        if (!todo) {
            res.status(404).send('Data is not found');
        } else {
            todo.title = req.body.title;
            todo.completed = req.body.completed;

            todo.save().then(todo => {
                res.json('Todo updated');
            })
            .catch(err => {
                res.status(400).send("Update not possible");
            });
        }
    });
});

todoRoutes.route('/delete/:id').delete(function(req, res) {
    Todo.findById(req.params.id, function(err, todo) {
        if (!todo) {
            res.status(404).send('Data is not found');
        } else {
            todo.remove().then(todo => {
                res.json('Todo successfully deleted');
            })
            .catch(err => {
                res.status(400).send("Delete not possible");
            });
        }
    });
});

app.use('/todos', todoRoutes);

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));